### 配置
```
    'aliases' => [
        '@jjl/yii2/db' => dirname(dirname(__DIR__)) . '/jjl/yii2db/src',
    ],
    'components' => [
        'dbLog' => [
            'class' => 'jjl\yii2\db\DbLog',
            'db' => 'db_log',
        ],
        'dbCache' => [
            'class' => 'jjl\yii2\db\Redis',
            'index' => [
                'master' => 7,
                'log' => 8,
            ]
        ]
	],
```

### 使用
```
// 创建基类集成核心类
abstract class Base extends \jjl\yii2\db\Core {

}

class Material extends Base {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'material';
    }

}
```

### 创建Log数据库
> 创建于业务数据同名表，语句如下：

```
CREATE TABLE `material`  (
  `id` int(11) UNSIGNED NOT NULL,
  `uuid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `object` json NOT NULL,
  `operator` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `time` int(11) UNSIGNED NOT NULL,
  `type` enum('insert','update','delete','recovery') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;
```