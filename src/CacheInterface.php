<?php

namespace jjl\yii2\db;

interface CacheInterface {

    /**
     * @param string $tag
     * @return $this
     */
    public function store(string $tag);

    /**
     * @param string $key
     * @param string|array $value
     * @param array $options
     * @return void
     */
    public function set(string $key, $value, array $options = []);

    /**
     * @param string $key
     * @param array $options
     */
    public function get(string $key);

    /**
     * @param string $key
     * @param array $options
     * @return void
     */
    public function del(string $key);
}
