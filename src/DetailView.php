<?php

namespace jjl\yii2\db;

use Yii;

class DetailView extends \yii\widgets\DetailView {

    public $templet = ['uuid'];
    public $hidden = ['id'];

    public function init() {
        if ($this->attributes && is_array($this->attributes)) {
            foreach ($this->attributes as $key => $value) {
                if (is_string($value) && in_array($value, $this->hidden)) {
                    unset($this->attributes[$key]);
                    continue;
                }
                if (is_string($value) && in_array($value, $this->templet)) {
                    $this->attributes[$key] = call_user_func(array($this, $value));
                }
            }
            parent::init();
        }
    }

    private function uuid() {
        return [
            'attribute' => 'uuid',
            'label' => Yii::t('yii2db', 'UUID'),
            'format' => 'raw',
            'value' => function($model) {
                $value = $model->uuid;
                if ($model->invalid)
                    $value .= '<span class="text-danger">（已失效）</span>';
                return $value;
            }
        ];
    }

}
