<?php

namespace jjl\yii2\db;

use Yii;

trait InitTrait {

    public function loadTranslations() {
        if (!isset(Yii::$app->i18n->translations['yii2db'])) {
            Yii::$app->i18n->translations['yii2db'] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'sourceLanguage' => 'en',
                'basePath' => '@jjl/yii2/db/messages',
            ];
        }
    }

}
