<?php

return [
    'WAL' => '命运多舛',
    'UUID' => '标识符',
    'Object' => '数据对象',
    'Operator' => '操作人',
    'Operation Time' => '操作时间',
    'Operation Type' => '操作类型',
];

